package com.klymchuk;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;


@WebServlet("/pizza")
public class PizzaServlet extends HttpServlet {
    private Logger logger = LogManager.getLogger(PizzaServlet.class);
    private Company company;


    @Override
    public void init() throws ServletException {
        company = new Company();
        logger.info("Init servlet\n");
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html");
        PrintWriter out = resp.getWriter();
        out.println("<html><body>");

        out.println("<p> <a href='pizza'>Refresh</a> </p>");

        for (Pizza pizza:company.getPizzas().values()){
            out.println("<p>"+pizza+"</p>");
        }

        out.println("<form action='pizzas' method='POST'>" +
                " Name: <input type='text' name='pizza_name'>\n" +
                " <button type='submit'>Add pizza</button>" +
                "</form>");


        out.println("</body></html>");
    }

}
