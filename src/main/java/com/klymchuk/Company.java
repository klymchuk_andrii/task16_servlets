package com.klymchuk;

import java.util.HashMap;
import java.util.Map;

public class Company {
    private Map<Integer,Pizza> pizzas;

    public Company(){
        pizzas = new HashMap<>();
        setPizzas(new Pizza("Carbonara"));
        setPizzas(new Pizza("La pez"));
    }

    public void setPizzas(Pizza pizza){
        pizzas.put(pizza.getId(),pizza);
    }

    public void removePizza(int id){
        pizzas.remove(id);
    }

    public Map<Integer, Pizza> getPizzas() {
        return pizzas;
    }
}
