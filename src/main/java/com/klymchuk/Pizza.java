package com.klymchuk;

public class Pizza {
    private String name;
    private static int counter = 1;
    private int id;

    public Pizza(String name) {
        this.name = name;
        this.id = counter;
        counter++;
    }

    public String getName() {
        return name;
    }

    public int getId() {
        return id;
    }

    @Override
    public String toString() {
        return id + " " + name;
    }
}
